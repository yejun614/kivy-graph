"""

    Vector
    ======
     - Vector Instance (vector.Vector)

    Author
    ======
     [developer] YeJun, Jung (yejunjung98@gmail.com)

    Copyright
    =========
     - MIT License
     - (c) 2019 kivy-graph project All right reserved.
     - https://gitlab.com/yejun614/kivy-graph

"""

MATH_VARIABLE_KEY = ['x', 'y', 'z', 'α', 'β', 'γ', 'θ']


"""
    Vector Instance Usage
    
    Vector(x=1, y=2, z=3)
    Vector([1, 2, 3])           # x=1, y=2, z=3
    Vector([1, 2, 3], t=50)     # x=1, y=2, z=3 and t=50
"""


class Vector:
    scalar = {}

    def __init__(self, scalars=list(), **args):
        if len(scalars) > 0:
            if len(scalars) > abs(len(MATH_VARIABLE_KEY) - len(self.scalar)):
                raise Exception('Too long number of scalars. (current: %d, length: %d, max: %d)' \
                                % (len(self.scalar), len(scalars), len(MATH_VARIABLE_KEY)))

            for i in range(len(scalars)):
                self.scalar[MATH_VARIABLE_KEY[i]] = scalars[i]

        for key in args:
            self.scalar[key] = args[key]

    def add_scalar(self, **args):
        for key in args:
            # TODO: Warning message will remove before release.
            if key in self.scalar.keys():
                print("Warning: '%s' key in scalar already." % key)

            self.scalar[key] = args[key]

    def _get_scalar(self, length):
        return [value for value in self.scalar.values()][0:length]
